import Vue from 'vue'
import firebase from 'firebase'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

// Initialize Firebase
var config = {
  apiKey: 'Your API Key',
  authDomain: 'vue-auth-firebase-485b0.firebaseapp.com',
  databaseURL: 'https://vue-auth-firebase-485b0.firebaseio.com',
  projectId: 'vue-auth-firebase-485b0',
  storageBucket: 'vue-auth-firebase-485b0.appspot.com',
  messagingSenderId: 'Your Message SenderID'
}
firebase.initializeApp(config)

let app = ''
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
})
